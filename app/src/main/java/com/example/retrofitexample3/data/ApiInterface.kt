package com.example.retrofitexample.data.remote

import com.example.retrofitexample3.data.PeopleDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("Name/{api_key}/{peopleId}")
    fun getDetailPeople(@Path("api_key") api_key: String, @Path("peopleId") peopleId: String): Call<PeopleDetail>

}