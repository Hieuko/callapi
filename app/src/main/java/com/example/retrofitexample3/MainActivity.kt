package com.example.retrofitexample3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    private var listId: ArrayList<String> = ArrayList<String>()
    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(MainRepository(this))
        listId.add("nm0000138")
        listId.add("nm0330687")
        listId.add("nm0680983")
        listId.add("nm0634240")
        presenter.getListPeople(listId)
    }
}