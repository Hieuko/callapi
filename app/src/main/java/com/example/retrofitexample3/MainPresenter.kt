package com.example.retrofitexample3

import android.util.Log
import com.example.retrofitexample.data.remote.ApiClient
import com.example.retrofitexample3.data.PeopleDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(
        private val repository: MainRepository
):MainContract.Presenter {
    override fun getListPeople(list: ArrayList<String>) {
        repository.callListPeople(list)
    }
}