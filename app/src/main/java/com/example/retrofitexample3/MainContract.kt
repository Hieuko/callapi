package com.example.retrofitexample3

interface MainContract {
    interface View{
        fun view()
    }

    interface Presenter{
        fun getListPeople(list: ArrayList<String>)
    }
}