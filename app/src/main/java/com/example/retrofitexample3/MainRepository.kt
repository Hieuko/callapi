package com.example.retrofitexample3

import android.content.Context
import android.util.Log
import com.example.retrofitexample.data.remote.ApiClient
import com.example.retrofitexample3.data.PeopleDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository(val context: Context) {
    var dataList = ArrayList<PeopleDetail>()

    fun callListPeople(list: ArrayList<String>) {
        for (i in list) {
            Log.d("TestAPI", "callListPeople() called with: PeopleDetail $i")
            val call: Call<PeopleDetail> =
                    ApiClient.getClient.getDetailPeople(context.getString(R.string.api_key), i)
            call.enqueue(object : Callback<PeopleDetail> {
                override fun onResponse(
                        call: Call<PeopleDetail>,
                        response: Response<PeopleDetail>
                ) {
                    if (response.isSuccessful) {
                        val mp = (response.body()) as PeopleDetail
                        Log.d("TestAPI", "callListPeople() called with: PeopleDetail $i = ${mp.toString()}")
                        dataList.add(mp)
                    }
                }

                override fun onFailure(call: Call<PeopleDetail>, t: Throwable) {
                    Log.d("Test_API", "onFailure() called with: call = $call, t = $t")
                }
            })
        }
        Log.d("TestAPI", "callListPeople() called with: list = ${dataList.toString()}")
    }
}